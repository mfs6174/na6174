/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: LU
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;

#include "NA6174.h"
#include "Mat.h"

void gaussSeidel(Mat &A,double xx[],double eps,int maxIter)
{
  int n=A.row;
  int k=0;
  double delta=INF;
  while (k<=maxIter&&delta>eps)
  {
    k++;
    delta=0;
    for (int i=1;i<=n;i++)
    {
      double tmp=xx[i];
      xx[i]=0;
      for (int j=1;j<=n;j++)
        if (i!=j)
          xx[i]-=A.at(i,j)*xx[j];
      xx[i]+=A.at(i,n+1);
      if (abs(A.at(i,i) )<EPS)
        xx[i]=INF;
      else
        xx[i]/=A.at(i,i);
      delta+=abs(xx[i]-tmp);
    }
  }
  if (delta<=eps)
    cout<<k<<endl;
}

double ss[]={48,-24,0,-12,4,-24,24,12,12,4,0,6,20,2,-2,-6,6,2,16,-2};
Mat AA(4,5,ss);
double xx[10];
int mi;

int main()
{
  cin>>mi;
  gaussSeidel(AA,xx,EPS,mi);
  for (int i=1;i<=4;i++)
    pf("%.8lf  ",xx[i]);
  cout<<endl;
  return 0;
}

