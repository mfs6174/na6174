/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: ti
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;

#include "NA6174.h"
#define MAXN 10
struct newtonInter
{
  int n;
  double xi[MAXN],yi[MAXN],f[MAXN][MAXN];
  void init(int c,double xx[],double yy[])
  {
    n=c;
    for (int i=0;i<=n;i++)
    {
      xi[i]=xx[i];
      yi[i]=yy[i];
      f[i][i]=yi[i];
    }
    for (int k=1;k<=n;k++)
      for (int i=0;i<=n-k;i++)
        f[i][i+k]=(f[i+1][i+k]-f[i][i+k-1])/(xi[i+k]-xi[i]);
  }
  double calc(double x)
  {
    if (n<1)
    {
      cout<<"uninit"<<endl;
      return INF;
    }
    double ret=0;
    for (int k=0;k<=n;k++)
    {
      double tmp=f[0][k];
      for (int i=0;i<=k-1;i++)
        tmp*=x-xi[i];
      ret+=tmp;
    }
    return ret;
  }
};

struct lagrangeInter
{
  int n;
  double xi[MAXN],yi[MAXN];
  void init(int c,double xx[],double yy[])
  {
    n=c;
    for (int i=0;i<=n;i++)
    {
      xi[i]=xx[i];
      yi[i]=yy[i];
    }
  }
  double calc(double x)
  {
    if (n<1)
    {
      cout<<"uninit"<<endl;
      return INF;
    }
    double ret=0;
    for (int k=0;k<=n;k++)
    {
      double lk=1;
      for (int i=0;i<=n;i++)
        if (i!=k)
          lk*=(x-xi[i])/(xi[k]-xi[i]);
      ret+=lk*yi[k];
    }
    return ret;
  }
};

lagrangeInter Ln;
newtonInter Nn;
double mx[]={-3,1,2},my[]={1,2,2};

int main()
{
  Ln.init(2,mx,my);
  Nn.init(2,mx,my);
  cout<<Ln.calc(-2)<<endl;
  cout<<Ln.calc(0)<<endl;
  cout<<Ln.calc(2.75)<<endl;

  cout<<Nn.calc(-2)<<endl;
  cout<<Nn.calc(0)<<endl;
  cout<<Nn.calc(2.75)<<endl;
  
  return 0;
}
