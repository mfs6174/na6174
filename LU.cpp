/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: LU
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;

#include "NA6174.h"
#include "Mat.h"

void LU(Mat &A,bool flag)
{
  int n=A.row;
  for (int k=1;k<=n;k++)
  {
    if (flag)
    {
      double s=-INF,sn;
      int p;
      for (int i=k;i<=n;i++)
      {
        sn=A.at(i,k);
        for (int j=1;j<=k-1;j++)
          sn-=A.at(i,j)*A.at(j,k);
        if (sn>s)
        {
          s=sn;
          p=i;
        }
      }
      for (int i=1;i<=n+1;i++)
      {
        double tmp=A.at(k,i);
        A.at(k,i)=A.at(p,i);
        A.at(p,i)=tmp;
      }
    }
    for (int j=k;j<=n+1;j++)
      for (int q=1;q<=k-1;q++)
        A.at(k,j)-=A.at(k,q)*A.at(q,j);
    for (int i=k+1;i<=n;i++)
    {
      for (int q=1;q<=k-1;q++)
        A.at(i,k)-=A.at(i,q)*A.at(q,k);
      A.at(i,k)/=A.at(k,k);
    }
  }
}

void subs(Mat &src,double x[])
{
  int n=src.row;
  for (int i=n;i>=1;i--)
  {
    x[i]=src.at(i,n+1);
    for (int j=i+1;j<=n;j++)
      x[i]-=src.at(i,j)*x[j];
    x[i]/=src.at(i,i);
  }
}

double ss[]={48,-24,0,-12,4,-24,24,12,12,4,0,6,20,2,-2,-6,6,2,16,-2};
Mat AA(4,5,ss);
double xx[10];

int main()
{
  LU(AA,1);
  AA.print();
  subs(AA,xx);
  for (int i=1;i<=4;i++)
    pf("%.8lf  ",xx[i]);
  cout<<endl;
  return 0;
}

