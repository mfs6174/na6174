/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: 1.4
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
ifstream inf("ti.in");
const int maxlongint=2147483647;

int i,j,n;
double sn,rsn;
double calcS(int n,bool flag)
{
  double sn=0;
  if (flag)
    for (i=n;i>=2;i--)
      sn+=1.0/(i*i-1);
  else
    for (i=2;i<=n;i++)
      sn+=1.0/(i*i-1);
  return sn;
}

double calcRS(int n)
{
  return 0.5*(1.5-1.0/n-1.0/(n+1));
}

int main()
{
  pf("%.10lf   %.10lf   %.10lf\n",calcS(1000,0),calcS(1000,1),calcRS(1000));
  pf("%.10lf   %.10lf   %.10lf\n",calcS(10000,0),calcS(10000,1),calcRS(10000));
  pf("%.10lf   %.10lf   %.10lf\n",calcS(30000,0),calcS(30000,1),calcRS(30000));
}
