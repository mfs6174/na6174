/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: Newton.cpp
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;

typedef double (*NTFunc)( const double x);
typedef double (*NTDFunc)( const double x);

double func1(const double x)
{
  return x*x-exp(x);
}

double dfunc1(const double x)
{
  return 2*x-exp(x);
}

double NewtonIter(NTFunc func,NTDFunc dfunc,double x0,double eps,int maxIter)
{
  double xx=x0;
  int it=0;
  while (it<=maxIter && abs(func(xx)/dfunc(xx))>=eps)
  {
    it++;
    xx-=func(xx)/dfunc(xx);
  }
  return xx;
}

double x0,eps,xx;
int maxIter;
int main()
{
  eps=1e-9;
  cin>>x0>>maxIter;
  xx=NewtonIter(func1,dfunc1,x0,eps,maxIter);
  pf("%.9lf  %.9lf\n",xx,func1(xx));
  return 0;
}
