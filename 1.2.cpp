/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: 1.2
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;
#define MAXN 110000

int i,j,n;
double a[MAXN],b[MAXN],dd;

int main()
{
  cin>>n;
  for (int i=0;i<=n;i++)
    cin>>b[i];
  for (int i=0;i<=n;i++)
    cin>>a[i];
  dd=b[n];
  for (int i=n-1;i>=0;i--)
    dd=b[i]+a[i+1]/dd;
  printf("f= %.6lf",dd);
  return 0;
}
