/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: ti
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;

double In;
int main()
{
  In=log(5)/4.0;
  cout<<In<<endl;
  for (int i=1;i<=10;i++)
  {
    In=1.0/(4*i)-In/4;
    cout<<In<<endl;
  }
  cout<<endl;
  In=(1.0/11+1.0/55)/2;
  cout<<In<<endl;
  for (int i=10;i>=1;i--)
  {
    In=1.0/i-4*In;
    cout<<In<<endl;
  }
  return 0;
}
  
