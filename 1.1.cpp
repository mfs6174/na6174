/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: NumericalAnalysis_1.1
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;
#include "NA6174.h"

float ss;
int main()
{
  ss=0;
  for (int i=1;i<=10000;i++)
    ss+=1.0/(i*i);
  pf("%f\n",abs(ss-1.644834));
  ss=0;
  for (int i=10000;i>=1;i--)
    ss+=1.0/(i*i);
  pf("%f\n",abs(ss-1.644834));
}

