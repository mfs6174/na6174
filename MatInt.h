#ifndef MAT_H
#define MAT_H

struct Mat
{
  int row,col;
  vector<vector<llg> > data;
  Mat()
  {
    row=col=0;
    data.clear();
  }
  Mat(int n,int m,llg s[])
  {
    row=n; col=m;
    data.clear();
    for (int i=1;i<=n;i++)
    {
        data.push_back(vector<llg>());
        for (int j=1;j<=m;j++)
          data[i-1].push_back(s[(i-1)*m+j-1]);
    }
  }
  ~Mat()
  {
    data.clear();
  }
  Mat(const Mat &s)
  {
    row=s.row;
    col=s.col;
    data=s.data;
  }
  llg &at(int n,int m)
  {
    if (n>=1&&n<=row&&m>=1&&m<=col)
      return (data[n-1][m-1]);
    else
    {
      cout<<"Mat Index Error"<<endl;
      exit(-1);
    }
  }
  void print(void)
  {
    for (int i=1;i<=row;i++)
    {
      for (int j=1;j<=col;j++)
        cout<<at(i,j)<<' ';
      cout<<endl;
    }
    cout<<endl;
  }
};

#endif
