/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: LU
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf

using namespace std;

#include "NA6174.h"
#include "Mat.h"

void LU(Mat &A,bool flag)
{
  int n=A.row,m=A.col;
  for (int k=1;k<=n;k++)
  {
    if (flag)
    {
      double s=-INF,sn;
      int p;
      for (int i=k;i<=n;i++)
      {
        sn=A.at(i,k);
        for (int j=1;j<=k-1;j++)
          sn-=A.at(i,j)*A.at(j,k);
        if (sn>s)
        {
          s=sn;
          p=i;
        }
      }
      for (int i=1;i<=m;i++)
      {
        double tmp=A.at(k,i);
        A.at(k,i)=A.at(p,i);
        A.at(p,i)=tmp;
      }
    }
    for (int j=k;j<=m;j++)
      for (int q=1;q<=k-1;q++)
        A.at(k,j)-=A.at(k,q)*A.at(q,j);
    for (int i=k+1;i<=n;i++)
    {
      for (int q=1;q<=k-1;q++)
        A.at(i,k)-=A.at(i,q)*A.at(q,k);
      A.at(i,k)/=A.at(k,k);
    }
  }
}

double det(Mat &src)
{
  double ret=1;
  int n=src.row;
  for (int i=1;i<=n;i++)
    ret=(ret*src.at(i,i));
  return ret;
}

ifstream fin("data.in");

int n,i,j;
double ss[1000];
int main()
{
  fin>>n;
  for (i=0;i<n*n;i++)
    fin>>ss[i];
  Mat AA(n,n,ss);
  AA.print();
  LU(AA,1);
  AA.print();
  double rdet=det(AA);
  cout<<rdet<<endl;
  return 0;
}
