/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: ti
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;

#include "NA6174.h"
typedef double (*DFunc)(const double x,const double y);
typedef double (*Solver)(double xi,double yi,double fh,DFunc func);

double f1(const double x,const double y)
{
  return x*x+y*y;
}

double modEuler(double xi,double yi,double fh,DFunc func)
{
  double yp=yi+fh*func(xi,yi);
  return (yp+yi+fh*func(xi+fh,yp) )/2;
}

double rungeKutta(double xi,double yi,double fh,DFunc func)
{
  double k=func(xi,yi),sumK=0;
  sumK+=k;
  k=func(xi+fh/2,yi+fh/2*k);
  sumK+=k*2;
  k=func(xi+fh/2,yi+fh/2*k);
  sumK+=k*2;
  k=func(xi+fh,yi+fh*k);
  sumK+=k;
  return yi+fh/6*sumK;
}

void odeSolver(double x0,double y0,double xb,double fh,Solver solv,DFunc func,vector<double> &rst)
{
  rst.clear();
  if (abs(fh)<=EPS)
  {
    cout<<"h can not be 0"<<endl;
    return;
  }
  double xx=x0,yy=y0;
  rst.push_back(yy);
  while (xx<=xb)
  {
    yy=solv(xx,yy,fh,func);
    rst.push_back(yy);
    xx+=fh;
  }
}
vector<double> rr;

int main()
{
  odeSolver(0,0,1,0.1,modEuler,f1,rr);
  for (int i=0;i<rr.size();i++)
    pf("%.6lf\n",rr[i]);
  cout<<endl;
  odeSolver(0,0,1,0.1,rungeKutta,f1,rr);
  for (int i=0;i<rr.size();i++)
    pf("%.6lf\n",rr[i]);
  cout<<endl;
  return 0;
}
