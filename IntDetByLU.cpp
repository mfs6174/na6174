/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: LU
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf

using namespace std;

#include "NA6174.h"
#include "MatInt.h"



const llg mod=1317131;

void extend_Euclid(llg a, llg b,llg &x,llg &y)
{
    if(b==0)
    {
        x = 1;
        y = 0;
        return;
    }
    extend_Euclid(b, a%b,x,y);
    llg t = x;
    x = y;
    y = t - a/b*y;
}

int LU(Mat &A,bool flag)
{
  int swapCnt=0;
  int n=A.row,m=A.col;
  for (int k=1;k<=n;k++)
  {
    if (flag)
    {
      llg s=-maxlongint,sn;
      llg p;
      for (int i=k;i<=n;i++)
      {
        sn=A.at(i,k);
        for (int j=1;j<=k-1;j++)
          sn=((sn-(A.at(i,j)*A.at(j,k))%mod)%mod+mod)%mod;
        if (sn>s)
        {
          s=sn;
          p=i;
        }
      }
      if (k!=p)
        swapCnt++;
      for (int i=1;i<=m;i++)
      {
        llg tmp=A.at(k,i);
        A.at(k,i)=A.at(p,i);
        A.at(p,i)=tmp;
      }
    }
    for (int j=k;j<=m;j++)
      for (int q=1;q<=k-1;q++)
        A.at(k,j)=((A.at(k,j)-A.at(k,q)*A.at(q,j)%mod)%mod+mod)%mod;
    for (int i=k+1;i<=n;i++)
    {
      for (int q=1;q<=k-1;q++)
        A.at(i,k)=((A.at(i,k)-A.at(i,q)*A.at(q,k)%mod)%mod+mod)%mod;
      llg x=0,y=0;
      extend_Euclid(A.at(k,k),mod,x,y);
      x=(x%mod+mod)%mod;
      A.at(i,k)=((A.at(i,k)*x)%mod+mod)%mod;
    }
  }
  return swapCnt;
}



llg det(Mat &src)
{
  int sc=LU(src,1);
  src.print();
  llg ret=1;
  int n=src.row;
  for (int i=1;i<=n;i++)
    ret=((ret*src.at(i,i))%mod+mod)%mod;
  if (sc&1)
    ret=-ret;
  return (ret%mod+mod)%mod;
}

ifstream fin("data1.in");

int n,i,j;
llg ss[1000];
int main()
{
  fin>>n;
  for (i=0;i<n*n;i++)
    fin>>ss[i];
  Mat AA(n,n,ss);
  AA.print();
  llg rdet=det(AA);
  cout<<rdet<<endl;
  return 0;
}

