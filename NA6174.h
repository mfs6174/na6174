#ifndef NA6174_H
#define NA6174_H

#define llg long long 

const double PI=3.14159265358979323846264338328; 
const double EPS=1e-9;
const double INF=1e200;
const int maxlongint=2147483647;

#endif
