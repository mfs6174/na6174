/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: ti
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;

#include "NA6174.h"
typedef double (*IntFunc)( const double x);
typedef double (*Integrator)(double xa,double xb,IntFunc func);

double f1(const double x)
{
  return 1/(1+x*x);
}

double f2(const double x)
{
  if (abs(x)<EPS)
    return 1.0;
  else
    return sin(x)/x;
}

inline double trapInt(double xa,double xb,IntFunc func)
{
  return (xb-xa)/2*(func(xa)+func(xb));
}

inline double simpsonInt(double xa,double xb,IntFunc func)
{
  return (xb-xa)/6*(func(xa)+4*func((xb+xa)/2)+func(xb));
}

inline double cotesInt(double xa,double xb,IntFunc func)
{
  double h=(xb-xa)/4;
  return (xb-xa)/90*(7*func(xa)+32*func(xa+h)+12*func(xa+2*h)+32*func(xa+3*h)+7*func(xb) );
}

double reIntegrator(double xa,double xb,IntFunc func,int n,Integrator intor)
{
  double dh=(xb-xa)/n;
  double intSum=0;
  for (int i=0;i<n;i++)
    intSum+=intor(xa+i*dh,xa+(i+1)*dh,func);
  return intSum;
}

int main()
{
  int n1,n2,n3;
  cin>>n1>>n2>>n3;
  for (int i=1;i<=n1;i++)
    pf("%.6lf\n",reIntegrator(0,1,f2,i,trapInt));
  cout<<endl;
  for (int i=1;i<=n2;i++)
    pf("%.6lf\n",reIntegrator(0,1,f2,i,simpsonInt));
  cout<<endl;
  for (int i=1;i<=n3;i++)
    pf("%.6lf\n",reIntegrator(0,1,f2,i,cotesInt));
  cout<<endl;
  return 0;
}
  
