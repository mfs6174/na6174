/*
ID: mfs6174
email: mfs6174@gmail.com
PROG: Newton.cpp
LANG: C++
*/

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<cstring>
#include<algorithm>
#include<map>
#include<vector>
#include<queue>
#include<deque>
#include<iomanip>
#include<cmath>
#include<set>
#define sf scanf
#define pf printf
#define llg long long 

using namespace std;
const int maxlongint=2147483647;

typedef double (*STFunc)( const double x);

double func1(const double x)
{
  return x*x-exp(x);
}

double SecantIter(STFunc func,double x0,double x1,double eps,int maxIter)
{
  double xx[2]={x0,x1};
  int it=0,id=1;
  while (it<=maxIter && abs(func(xx[id])*(xx[id]-xx[1-id])/(func(xx[id])-func(xx[1-id]) ) )>=eps)
  {
    it++;
    xx[1-id]=xx[id]-func(xx[id])*(xx[id]-xx[1-id])/(func(xx[id])-func(xx[1-id]) );
    id=1-id;
  }
  return xx[id];
}

double x0,eps,xx,x1;
int maxIter;
int main()
{
  eps=1e-9;
  cin>>x0>>x1>>maxIter;
  xx=SecantIter(func1,x0,x1,eps,maxIter);
  pf("%.9lf  %.9lf\n",xx,func1(xx));
  return 0;
}
